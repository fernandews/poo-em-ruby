require_relative "person.rb"
class Student < Person
    def initialize
        super
        @is_at_school = true
        puts "Qual a sua matrícula?"
        @mat = gets
    end
    def presentation
        yield @name, @cpf, @age, @birth, @address, @is_at_school, @mat
    end
    def go
        if @is_at_school == true
            puts "Eu estou na escola, mas já vou para casa"
            puts "-" * 20
            puts "Andando..."
            puts "-" * 20
            @is_at_school = false
            puts "Já cheguei em casa."
        end
    end
end 
s = Student.new
s.presentation do |name, cpf, age, birth, address, is_at_school, mat|
    puts "Meu nome é #{name.capitalize.strip}, meu cpf é #{cpf.strip}, tenho #{age.strip} anos, nasci no dia #{birth.strip} e moro em #{address}. Minha matrícula é #{mat.strip}."
end
puts " "
s.go